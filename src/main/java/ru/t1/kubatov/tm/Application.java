package ru.t1.kubatov.tm;

import ru.t1.kubatov.tm.component.Bootstrap;

public final class Application {

    public static void main(final String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
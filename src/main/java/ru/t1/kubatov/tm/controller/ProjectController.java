package ru.t1.kubatov.tm.controller;

import ru.t1.kubatov.tm.api.controller.IProjectController;
import ru.t1.kubatov.tm.api.service.IProjectService;
import ru.t1.kubatov.tm.api.service.IProjectTaskService;
import ru.t1.kubatov.tm.model.Project;
import ru.t1.kubatov.tm.model.Task;
import ru.t1.kubatov.tm.util.TerminalUtil;
import ru.t1.kubatov.tm.enumerated.Status;

import java.util.List;
import java.util.Arrays;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    private final IProjectTaskService projectTaskService;

    public ProjectController(IProjectService projectService, IProjectTaskService projectTaskService) {
        this.projectService = projectService;
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");
        final List<Project> projects = projectService.findAll();
        showProjectList(projects);
        System.out.println("[OK]");
    }

    public void showProjectList(final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            System.out.printf("%s[%s]. %s -\n", index, project.getId(), project);
            index++;
        }
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) System.err.println("[ERROR]");
        else System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[PROJECT CLEAR]");
        projectTaskService.removeProjects();
        System.out.println("[OK]");
    }

    @Override
    public void deleteProjectByID() {
        System.out.println("[DELETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.deleteByID(id);
        if (project == null) System.err.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void deleteProjectByIndex() {
        System.out.println("[DELETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.deleteByIndex(index);
        if (project == null) System.err.println("[FAIL]");
        else System.out.println("[OK]");
    }

    private void showProject(final Project project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Override
    public void showProjectByID() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findByID(id);
        if (project == null) {
            System.err.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.findByIndex(index);
        if (project == null) {
            System.err.println("[FAIL]");
            return;
        }
        showProject(project);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByID() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByID(id, name, description);
        if (project == null) System.err.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) System.err.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeProjectStatusById(id, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = projectService.changeProjectStatusByIndex(index, Status.COMPLETED);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusById(id, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS BY INDEX]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.toStatus(statusValue);
        final Project project = projectService.changeProjectStatusByIndex(index, status);
        if (project == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}

package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    Task add(Task task);

    void deleteAll();

    Task findByID(String id);

    Task findByIndex(Integer index);

    List<Task> findAllByProjectID(String projectId);

    Task delete(Task task);

    Task deleteByID(String id);

    Task deleteByIndex(Integer index);

}

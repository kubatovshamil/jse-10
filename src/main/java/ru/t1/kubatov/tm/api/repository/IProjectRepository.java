package ru.t1.kubatov.tm.api.repository;

import ru.t1.kubatov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    Project add(Project project);

    void deleteAll();

    boolean existsById(String id);

    Project findByID(String id);

    Project findByIndex(Integer index);

    Project delete(Project project);

    Project deleteByID(String id);

    Project deleteByIndex(Integer index);

}
